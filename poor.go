package breaking // import "gitlab.com/vearutop/breaking"
import "errors"

// MyPublic is a publicly available struct
type MyPublic struct {
	Param string `json:"param" db:"param"`
}

// DoWhatever does whatever it does
func (m MyPublic) DoWhatever(a MyPublic) (error, MyPublic) {
	return Helper{"EN"}.GetError("ouch"), MyPublic{Param: a.Param}
}

// Helper is a imaginary friend
type Helper struct{ Lang string }

// GetError gets error
func (h Helper) GetError(text string) error { return errors.New(h.Lang + text) }
